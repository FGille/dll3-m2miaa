navigator.mediaDevices.getUserMedia({
    video: true
  }).then(stream => {
    document.querySelector('#status').innerHTML =
      'Nous avons eu acces a votre camera, merci. Vous pouvez fermer cet onglet.';
    chrome.storage.local.set({
      'camAccess': true
    }, () => {});
  })
  .catch(err => {
    document.querySelector('#status').innerHTML =
      'Error getting webcam access for extension: ' + err.toString();
    console.error(err);
  });
