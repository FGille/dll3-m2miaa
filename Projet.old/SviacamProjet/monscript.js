 // assign global variables to HTML elements
    var video = document.getElementById( 'webcamVideo' );
    var videoCanvas = document.getElementById( 'videoCanvas' );
    var videoContext = videoCanvas.getContext( '2d' );

    var layer2Canvas = document.getElementById( 'layer2' );
    var layer2Context = layer2Canvas.getContext( '2d' );

    var blendCanvas  = document.getElementById( "blendCanvas" );
    var blendContext = blendCanvas.getContext('2d');

    var messageArea = document.getElementById( "messageArea" );

    // these changes are permanent
    videoContext.translate(320, 0);
    videoContext.scale(-1, 1);

    // background color if no video present
    videoContext.fillStyle = '#005337';
    videoContext.fillRect( 0, 0, videoCanvas.width, videoCanvas.height );

    var buttons = [];
    var large = 40 ;
    var haut = 40 ;
    var img ="images/img.png"
    // ACTIVE CANVAS 1
        function checking1 (check) {
            if (check.checked)
              {
                // document.getElementById("canvas1").style.display="block";
              // var button1 = document.getElementById('layer1');
              var button1 = new Image();
               button1.src =img;
              var buttonData1 = { name:"red", image:button1, x:131 - 96 - 30, y:5, w:large, h:haut };
              // var buttonData1 = { name:"red"};
              buttons.push( buttonData1 );

              document.getElementById("layerA").style.display="block";
            }
              else {
                for(var i = buttons.length -1; i>=0; i--)
                {
                  if(buttons[i].name === "red")
                  {
                    buttons.splice( i , 1);
                  }
                }
                document.getElementById("layerA").style.display="none";
              }
        };

        // ACTIVE CANVAS 2
            function checking2 (check) {
                if (check.checked)
                {
                  // var button2 = document.getElementById('layer2');
                 var button2 = new Image();
                button2.src =img;
                  var buttonData2 = { name:"green", image:button2, x:131 - 96 - 30, y:85, w:large, h:haut };
                 buttons.push( buttonData2 );

                 document.getElementById("layerC").style.display="block";
               }
               else {
                 for(var i = buttons.length -1; i>=0; i--)
                 {
                   if(buttons[i].name === "green")
                   {
                     buttons.splice( i , 1);
                   }
                 }
                 document.getElementById("layerC").style.display="none";
               }
            };

            // ACTIVE CANVAS 3
                function checking3 (check) {
                    if (check.checked)
                    {
                      // var button3 = document.getElementById('layer3');
                    var button3 = new Image();
                    button3.src =img;
                     var buttonData3 = { name:"blue", image:button3, x:312 - 32 - 10, y:5, w:large, h:haut };
                    buttons.push( buttonData3 );

                    document.getElementById("layerB").style.display="block";
                  }
                  else {
                    for(var i = buttons.length -1; i>=0; i--)
                    {
                      if(buttons[i].name === "blue")
                      {
                        buttons.splice( i , 1);
                      }
                    }

                    document.getElementById("layerB").style.display="none";
                  }
                };

                // ACTIVE CANVAS 4
                    function checking4 (check) {
                        if (check.checked)
                        {
                          // var button3 = document.getElementById('layer3');
                        var button4 = new Image();
                        button4.src =img;
                         var buttonData4 = { name:"yellow", image:button4, x:312 - 32 - 10, y:85, w:large, h:haut };
                        buttons.push( buttonData4 );

                        document.getElementById("layerD").style.display="block";
                      }
                      else {
                        for(var i = buttons.length -1; i>=0; i--)
                        {
                          if(buttons[i].name === "yellow")
                          {
                            buttons.splice( i , 1);
                          }
                        }

                        document.getElementById("layerD").style.display="none";
                      }
                    };



    function animate()
    {
      requestAnimationFrame( animate );
      render();
      blend();
      checkAreas();
    }
    // start the loop
    animate();

    function render()
    {
      if ( video.readyState === video.HAVE_ENOUGH_DATA )
      {
        // mirror video
        videoContext.drawImage( video,0,0,videoCanvas.width, videoCanvas.height );
        for ( var i = 0; i < buttons.length; i++ )
          layer2Context.drawImage( buttons[i].image, buttons[i].x, buttons[i].y, buttons[i].w, buttons[i].h );
      }
    }

    var lastImageData;

    function blend()
    {
      var width  = videoCanvas.width;
      var height = videoCanvas.height;
      // get current webcam image data
      var sourceData = videoContext.getImageData(0, 0, width, height);
      // create an image if the previous image doesn�t exist
      if (!lastImageData) lastImageData = videoContext.getImageData(0, 0, width, height);
      // create a ImageData instance to receive the blended result
      var blendedData = videoContext.createImageData(width, height);
      // blend the 2 images
      differenceAccuracy(blendedData.data, sourceData.data, lastImageData.data);
      // draw the result in a canvas
      blendContext.putImageData(blendedData, 0, 0);
      // store the current webcam image
      lastImageData = sourceData;
    }


    function differenceAccuracy(target, data1, data2)
    {
      if (data1.length != data2.length) return null;
      var i = 0;
      while (i < (data1.length * 0.25))
      {
        var average1 = (data1[4*i] + data1[4*i+1] + data1[4*i+2])/3;
        var average2 = (data2[4*i] + data2[4*i+1] + data2[4*i+2])/3;
        var diff = threshold(fastAbs(average1 - average2));
        target[4*i]   = diff;
        target[4*i+1] = diff;
        target[4*i+2] = diff;
        target[4*i+3] = 0xFF;
        ++i;
      }
    }
    function fastAbs(value)
    {
      return (value ^ (value >> 31)) - (value >> 31);
    }
    function threshold(value)
    {
      return (value > 0x15) ? 0xFF : 0;
    }

    // check if white region from blend overlaps area of interest (e.g. triggers)
    function checkAreas()
    {
      for (var b = 0; b < buttons.length; b++)  {
        // get the pixels in a note area from the blended image
         var blendedData = blendContext.getImageData( buttons[b].x, buttons[b].y, buttons[b].w, buttons[b].h );
        // calculate the average lightness of the blended data
        var i = 0;
        var sum = 0;
        var countPixels = blendedData.data.length * 0.25;
        while (i < countPixels)
        {
          sum += (blendedData.data[i*4] + blendedData.data[i*4+1] + blendedData.data[i*4+2]);
          ++i;
        }
        // calculate an average between of the color values of the note area [0-255]
        var average = Math.round(sum / (3 * countPixels));
        if (average > 50) // more than 40% movement detected
        {

          if (buttons[b].name == "blue") {
            console.log( "Button " + buttons[b].name + " triggered." ); // do stuff
            window.history.back();

        }
          else if (buttons[b].name == "red") {
            console.log( "Button " + buttons[b].name + " triggered." ); // do stuff
            window.open('http://www.eurosport.fr','fullscreen=yes');

          }
          else if (buttons[b].name == "green")  {
            console.log( "Button " + buttons[b].name + " triggered." ); // do stuff
            window.scrollBy(0,100);
          }
          else if (buttons[b].name == "yellow")  {
            window.scrollBy(0,-100);
          }
        }
      }
    }



    document.getElementById("check1").addEventListener("click", checking1(this));
    document.getElementById("check2").addEventListener("click", checking2(this));
    document.getElementById("check3").addEventListener("click", checking3(this));
    document.getElementById("check4").addEventListener("click", checking4(this));